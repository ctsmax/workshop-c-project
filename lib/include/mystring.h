/*
** EPITECH PROJECT, 2020
** Myprints
** File description:
** Prototypes of the prints functions
*/

#ifndef MYSTRING_H
#define MYSTRING_H

int my_isalpha(char const c);
int my_islower(char const c);
int my_isnum(char const c);
int my_isprintable(char const c);
int my_str_isupper(char const *str);
int my_printf(const char *format, ...);
void my_putchar(const char c);
int my_put_nbr(int nb);
void my_putstr(char const *str);
char *my_revstr(char *str);
int my_show_word_array(char * const *tab);
void my_sort_word_array(char **tab);
char **my_str_to_word_array(char const *str);
char *my_strcapitalize(char *str);
char *my_strcat(char *dest, char const *src);
char *my_strconcat(const char *str1, char const *str2);
int my_strcmp(const char *str1, const char *str2);
int my_str_startwith(const char *to_find, const char *str);
char *my_strcpy(char *dest, char const *src);
char *my_strdup(char const *src);
int my_str_isalpha(char const *str);
int my_str_islower(char const *str);
int my_str_isnum(char const *str);
int my_str_isprintable(char const *str);
int my_isupper(char const c);
int my_strlen(char const *str);
char *my_strlowcase(char *str);
char *my_strncat(char *dest, char const *src, int nb);
char *my_strncpy(char *dest, char const *src, int n);
int my_str_numlen(char const *str);
char *my_strupcase(char *str);
int my_nbrlen(int nbr);
char *my_int_to_str(int nbr);
void *my_print_error(const char *str, int fd, void *ret);
int my_print_int_error(const char *str, int fd, int ret);

#endif /* MYSTRING_H */
