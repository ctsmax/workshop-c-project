/*
** EPITECH PROJECT, 2020
** My_isneg
** File description:
** Return 1 if the number is negative
*/

int my_isneg(int n)
{
    if (n < 0)
        return 1;
    return 0;
}
