/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** criterion tests
*/

#include <unistd.h>
#include "tests.h"

Test(matchstick, check_end_game_exit)
{
    char *map[] = {NULL};

    cr_assert_eq(check_endgame((const char **)map, EXIT), MYEXIT_SUCCESS);
}

Test(matchstick, check_end_game_notend)
{
    char *map[] = {
        "     ",
        " |   ",
        "     ",
        NULL
    };

    cr_assert_eq(check_endgame((const char **)map, USER), SPECIFIC);
}

Test(matchstick, check_end_game_yesend)
{
    char *map[] = {
        "     ",
        "     ",
        "     ",
        NULL
    };

    cr_assert_eq(check_endgame((const char **)map, USER), USER);
}

Test(matchstick, launch_game, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};

    init_gamestruct((const char **)av, &game);
    fwrite("1\n1\n3\n1\n2\n1\n3\n1\n2\n1\n3\n1\n2\n1\n3\n1\n", 1, 32, cr_get_redirected_stdin());
    cr_assert_neq(launch_game(game), SPECIFIC);
    free_map(game.map);
}

Test(matchstick, do_remove, .init = redirect_all_std)
{
    int i;
    char *line = malloc(sizeof(char) * (7));
    char tmp[] = "  ||   ";

    for (i = 0; tmp[i]; i++)
        line[i] = tmp[i];
    line[i] = '\0';
    do_remove(&line, 4, 1);
    cr_assert_stdout_eq_str(" removed 1 match(es) from line 4\n");
    cr_assert_str_eq(line, "  |    ");
    free(line);
}

Test(matchstick, do_remove_empty, .init = redirect_all_std)
{
    int i;
    char *line = malloc(sizeof(char) * (7));
    char tmp[] = "       ";

    for (i = 0; tmp[i]; i++)
        line[i] = tmp[i];
    line[i] = '\0';
    do_remove(&line, 4, 1);
    cr_assert_stdout_eq_str(" removed 1 match(es) from line 4\n");
    cr_assert_str_eq(line, "       ");
    free(line);
}

Test(matchstick, matchstick_usage, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "-h", NULL};

    cr_assert_eq(matchstick(2, (const char **)av), MYEXIT_SUCCESS);
}

Test(matchstick, matchstick_error_arg, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "str", "2", NULL};

    cr_assert_eq(matchstick(3, (const char **)av), MYEXIT_FAILURE);
}

Test(matchstick, matchstick_ai_win, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "2", "4", NULL};

    fwrite("2\n3\n", 1, 4, cr_get_redirected_stdin());
    cr_assert_eq(matchstick(3, (const char **)av), USER);
}

Test(matchstick, matchstick_user_win, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "2", "4", NULL};

    fwrite("2\n1\n1\n1\n2\n2\n", 1, 12, cr_get_redirected_stdin());
    cr_assert_eq(matchstick(3, (const char **)av), AI);
}

Test(matchstick, matchstick_exit, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "2", "4", NULL};

    fclose(cr_get_redirected_stdin());
    cr_assert_eq(matchstick(3, (const char **)av), MYEXIT_SUCCESS);
}
