/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** criterion tests
*/

#include <unistd.h>
#include "tests.h"

Test(ai, get_stickas_nbr)
{
    cr_assert_eq(3, get_sticks_nbr(" ||| "));
}

Test(ai, remove_line_more_than_matches, .init = redirect_all_std)
{
    int i;
    char *line = malloc(sizeof(char) * (6));
    char tmp[] = " ||| ";

    for (i = 0; tmp[i]; i++)
        line[i] = tmp[i];
    line[i] = '\0';
    remove_line(2, 1, 2, &line);
    cr_assert_str_eq(line, " ||  ");
    free(line);
}

Test(ai, remove_line_less_than_matches, .init = redirect_all_std)
{
    int i;
    char *line = malloc(sizeof(char) * (6));
    char tmp[] = " ||| ";

    for (i = 0; tmp[i]; i++)
        line[i] = tmp[i];
    line[i] = '\0';
    remove_line(1, 2, 2, &line);
    cr_assert_str_eq(line, " ||  ");
    free(line);
}

Test(ai, change_line_on_goal, .init = redirect_all_std)
{
    int i;
    char *line = malloc(sizeof(char) * (6));
    char tmp[] = "|||| ";

    for (i = 0; tmp[i]; i++)
        line[i] = tmp[i];
    line[i] = '\0';
    change_line(4, 2, 3, &line);
    cr_assert_str_eq(line, "|||  ");
    free(line);
}

Test(ai, change_line_attain_goal, .init = redirect_all_std)
{
    int i;
    char *line = malloc(sizeof(char) * (6));
    char tmp[] = "|||||";

    for (i = 0; tmp[i]; i++)
        line[i] = tmp[i];
    line[i] = '\0';
    change_line(5, 2, 3, &line);
    cr_assert_str_eq(line, "|||| ");
    free(line);
}

Test(ai, do_ai_move_more_lines, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};
    char *expected[] = {
        "     ",
        " ||| ",
        "|||||",
        NULL
    };

    init_gamestruct((const char **)av, &game);
    do_ai_move(&game);
    for (int i = 0; expected[i]; i++)
        cr_assert_str_eq(expected[i], game.map[i]);
    free_map(game.map);
}

Test(ai, do_ai_move_one_line, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};
    char *expected[] = {
        "     ",
        "     ",
        "|||| ",
        NULL
    };

    init_gamestruct((const char **)av, &game);
    for (int i = 0; i < 2; i++)
        for (int y = 0; game.map[i][y]; y++)
            game.map[i][y] = FREE_CHAR;
    do_ai_move(&game);
    for (int i = 0; expected[i]; i++)
        cr_assert_str_eq(expected[i], game.map[i]);
    free_map(game.map);
}
