/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Main functions for criterion tests
*/


#include "tests.h"

void redirect_all_std(void)
{
    cr_redirect_stderr();
    cr_redirect_stdout();
    cr_redirect_stdin();
}
