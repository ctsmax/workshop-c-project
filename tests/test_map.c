/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** criterion tests
*/


#include "tests.h"

Test(init_map, fill_map_line)
{
    game_t game;

    game.cols = 5;
    game.map = malloc(sizeof(char *) * 1);
    game.map[0] = malloc(sizeof(char) * 6);
    fill_map_line(&game, 0);
    cr_assert_str_eq("  |  ", game.map[0]);
    free(game.map[0]);
    free(game.map);
}

Test(init_map, init_gamestruct)
{
    int i;
    char *av[] = {"./matchstick", "3", "2", NULL};
    char *expected[] = {
        "  |  ",
        " ||| ",
        "|||||",
        NULL
    };
    game_t game;

    cr_assert_eq(init_gamestruct((const char **)av, &game), SUCCESS);
    for (i = 0; expected[i]; i++)
        cr_assert_str_eq(expected[i], game.map[i]);
    cr_assert_eq(NULL, game.map[i]);
    free_map(game.map);
}

Test(init_map, init_gamestruct_hard)
{
    int i;
    char *av[] = {"./matchstick", "6", "2", NULL};
    char *expected[] = {
        "     |     ",
        "    |||    ",
        "   |||||   ",
        "  |||||||  ",
        " ||||||||| ",
        "|||||||||||",
        NULL
    };
    game_t game;

    cr_assert_eq(init_gamestruct((const char **)av, &game), SUCCESS);
    for (i = 0; expected[i]; i++)
        cr_assert_str_eq(expected[i], game.map[i]);
    cr_assert_eq(NULL, game.map[i]);
    free_map(game.map);
}
