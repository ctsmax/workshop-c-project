/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** criterion tests
*/

#include <unistd.h>
#include "tests.h"

Test(user, check_valid_line)
{
    cr_assert_eq(check_line(3, "2"), SUCCESS);
}

Test(user, check_line_notnum, .init = redirect_all_std)
{
    cr_assert_eq(check_line(3, "str"), ERROR);
    cr_assert_stdout_eq_str(ERROR_INVALID_INPUT);
}

Test(user, check_line_negative, .init = redirect_all_std)
{
    cr_assert_eq(check_line(3, "-2"), ERROR);
    cr_assert_stdout_eq_str(ERROR_INVALID_INPUT);
}

Test(user, check_line_null, .init = redirect_all_std)
{
    cr_assert_eq(check_line(3, "0"), ERROR);
    cr_assert_stdout_eq_str(ERROR_INVALID_LINE);
}

Test(user, check_line_outofrange, .init = redirect_all_std)
{
    cr_assert_eq(check_line(3, "20"), ERROR);
    cr_assert_stdout_eq_str(ERROR_INVALID_LINE);
}

Test(user, get_player_valid_line, .init = redirect_all_std)
{
    fwrite("2\n", 1, 2, cr_get_redirected_stdin());
    cr_assert_eq(get_player_line(3), 2);
    cr_assert_stdout_eq_str("Line: ");
}

Test(user, get_player_invalid_line, .init = redirect_all_std)
{
    fwrite("20\n", 1, 3, cr_get_redirected_stdin());
    cr_assert_eq(get_player_line(3), ERROR);
}

Test(user, get_player_eof_line, .init = redirect_all_std)
{
    fclose(cr_get_redirected_stdin());
    cr_assert_eq(get_player_line(3), SPECIFIC);
}

Test(user, check_valid_matches)
{
    cr_assert_eq(check_matches(3, 3, "2"), SUCCESS);
}

Test(user, check_matches_notnum, .init = redirect_all_std)
{
    cr_assert_eq(check_matches(3, 3, "str"), ERROR);
    cr_assert_stdout_eq_str(ERROR_INVALID_INPUT);
}

Test(user, check_matches_negative, .init = redirect_all_std)
{
    cr_assert_eq(check_matches(3, 3, "-1"), ERROR);
    cr_assert_stdout_eq_str(ERROR_INVALID_INPUT);
}

Test(user, check_matches_null, .init = redirect_all_std)
{
    cr_assert_eq(check_matches(3, 3, "0"), ERROR);
    cr_assert_stdout_eq_str(ERROR_TOOLESS_MATCHES);
}

Test(user, check_matches_outofrange, .init = redirect_all_std)
{
    cr_assert_eq(check_matches(3, 3, "5"), ERROR);
    cr_assert_stdout_eq_str\
    ("Error: you cannot remove more than 3 matches per turn\n");
}

Test(user, check_matches_higher, .init = redirect_all_std)
{
    cr_assert_eq(check_matches(3, 1, "2"), ERROR);
    cr_assert_stdout_eq_str(ERROR_TOOMUCH_MATCHES);
}

Test(user, get_player_valid_matches, .init = redirect_all_std)
{
    fwrite("2\n", 1, 2, cr_get_redirected_stdin());
    cr_assert_eq(get_player_matches(3, " ||| "), 2);
    cr_assert_stdout_eq_str("Matches: ");
}

Test(user, get_player_invalid_matches, .init = redirect_all_std)
{
    fwrite("20\n", 1, 3, cr_get_redirected_stdin());
    cr_assert_eq(get_player_matches(3, " ||| "), ERROR);
}

Test(user, get_player_eof_matches, .init = redirect_all_std)
{
    fclose(cr_get_redirected_stdin());
    cr_assert_eq(get_player_matches(3, " ||| "), SPECIFIC);
}

Test(user, get_player_reads_valid, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};
    char *expected[] = {
        "  |  ",
        " ||| ",
        "|||  ",
        NULL
    };

    init_gamestruct((const char **)av, &game);
    fwrite("3\n2\n", 1, 4, cr_get_redirected_stdin());
    get_player_reads(&game);
    for (int i = 0; expected[i]; i++)
        cr_assert_str_eq(expected[i], game.map[i]);
    free_map(game.map);
}

Test(user, get_player_reads_invalid_firstline, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};
    char *expected[] = {
        "  |  ",
        " ||| ",
        "|||  ",
        NULL
    };

    init_gamestruct((const char **)av, &game);
    fwrite("40\n3\n2\n", 1, 7, cr_get_redirected_stdin());
    get_player_reads(&game);
    for (int i = 0; expected[i]; i++)
        cr_assert_str_eq(expected[i], game.map[i]);
    free_map(game.map);
}

Test(user, get_player_reads_invalid_firstmatches, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};
    char *expected[] = {
        "  |  ",
        " ||| ",
        "|||  ",
        NULL
    };

    init_gamestruct((const char **)av, &game);
    fwrite("3\n80\n3\n2\n", 1, 9, cr_get_redirected_stdin());
    get_player_reads(&game);
    for (int i = 0; expected[i]; i++)
        cr_assert_str_eq(expected[i], game.map[i]);
    free_map(game.map);
}

Test(user, get_player_reads_eof_firstline, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};

    init_gamestruct((const char **)av, &game);
    fclose(cr_get_redirected_stdin());
    get_player_reads(&game);
    cr_assert_eq(EXIT, game.cur_player);
    free_map(game.map);
}

Test(user, get_player_reads_eof_firstmatches, .init = redirect_all_std)
{
    game_t game;
    char *av[] = {"./matchstick", "3", "2", NULL};

    init_gamestruct((const char **)av, &game);
    fwrite("3\n", 1, 2, cr_get_redirected_stdin());
    fclose(cr_get_redirected_stdin());
    get_player_reads(&game);
    cr_assert_eq(EXIT, game.cur_player);
    free_map(game.map);
}
