/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Main functions for criterion tests
*/


#include "tests.h"

Test(usage, usage_print, .init = redirect_all_std)
{
    print_usage();
    cr_assert_stdout_eq_str("USAGE\n"
    "    ./matchstick [lines_nbr] [max_substitution]\n"
    "DESCRIPTION\n"
    "    lines_nbr: The number of lines the generated layout will have\n"
    "    max_substitution: The maximal number of sticks you can remove on one line\n");
}

Test(usage, usage_detect_yes, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "-h", NULL};

    cr_assert_eq(check_usage(2, (const char **)av), ERROR);
}

Test(usage, usage_detect_yes2, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "--help", NULL};

    cr_assert_eq(check_usage(2, (const char **)av), ERROR);
}

Test(usage, usage_detect_no, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "arg", NULL};

    cr_assert_eq(check_usage(2, (const char **)av), SUCCESS);
}
